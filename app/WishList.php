<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WishList extends Model
{
    public function savedProduct()
    {
        return $this->belongsTo(SavedProduct::class);
    }
    
}
