<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedProduct extends Model
{
    public function wishlist(){
        return $this->hasMany(Wishlist::class);
    }

    public function products(){
        return $this->belongsToMany(Product::class,'wish_lists','saved_product_id','product_id')->withPivot("quantity");
    }
}
