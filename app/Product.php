<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    public function categories()
    {
        return $this->belongsToMany(Category::class,'category_products','product_id','category_id');
    }

    public function wishlists(){
        return $this->belongsToMany(WishList::class,'wish_lists','saved_product_id','product_id')->withPivot("wish_lists", "quantity");
    }

}
