<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\SavedProduct;
use App\WishList;
use Auth;

class SavedProductController extends Controller
{
    public function saveCart(Request $req){
        $cart = $req->cart;

        if($cart){
            $uniqueId = uniqid('wishlist_');

            $wishlist = DB::insert('INSERT into saved_products (link) VALUES (?)', [$uniqueId]);

            $wId = DB::select('SELECT id FROM saved_products WHERE link = ?',[$uniqueId]);

            foreach($cart as $value){
                $request = DB::insert('INSERT INTO wish_lists (saved_product_id,product_id,quantity) VALUES (?,?,?)',[
                    $wId[0]->id,
                    $value['id'],
                    $value['quantity']]);
            };
            

            if($request){
                return [
                    'state' => 'success',
                    'link' => $uniqueId
                ];
            }
        }   
    }
    public function displayWishList($wishlist){

        $savedProduct = DB::select('SELECT * FROM saved_products WHERE link = ?',[$wishlist]);

        if($savedProduct){
            $products = DB::select('SELECT products.id, products.name, products.photo, products.variation, products.product_id, wish_lists.quantity as quantity, products.price FROM products
            INNER JOIN wish_lists ON products.id = wish_lists.product_id
            WHERE wish_lists.saved_product_id = ?',[$savedProduct[0]->id]);

            return $products;
        }

    }
    public function mergeCart(Request $req){
        $user = Auth::user();
        $responses = [];

        foreach ($req->cart as $key => $value) {
            if(DB::select('SELECT * FROM user_products WHERE user_id = ? AND product_id = ?',[$user->id,$value['id']])){
                $responses[$key] = DB::update('UPDATE user_products SET quantity = quantity + ? WHERE product_id = ? AND user_id = ?',[$value['quantity'],$value['id'],$user->id]);
            }else{
                $responses[$key] = DB::insert('INSERT INTO user_products (product_id,user_id,quantity) VALUES (?,?,?)',[$value['id'],$user->id,$value['quantity']]);
            }
        }

        return 'success';

    }
    public function replaceCart(Request $req){

        $user = Auth::user();
        $responses = [];

        DB::delete('DELETE FROM user_products WHERE user_id = ?',[$user->id]);

        foreach ($req->cart as $key => $value) {
            $responses[$key] = DB::insert('INSERT INTO user_products (product_id,user_id,quantity) VALUES (?,?,?)',[$value['id'],$user->id,$value['quantity']]);
        }

        return 'success';
    }
}
