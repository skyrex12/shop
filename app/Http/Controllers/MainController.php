<?php

namespace App\Http\Controllers;

class MainController extends Controller
{
    public function index(){
        $manifest = json_decode(file_get_contents(public_path('/manifest.json')), true);

        return view('main',['manifest'=>$manifest]);
    }
}