<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Product;
use App\Category;
use App\UserProduct;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class ProductController extends Controller
{
    public function index(Request $request)
    {        
        $currentPage = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        $perPage = 9;
        $offset = ($currentPage-1)*$perPage;

        $result = DB::select("SELECT COUNT(DISTINCT products.name) as count FROM PRODUCTS");
        $lastPage = ceil($result[0]->count / $perPage);

        $queryBeg = "SELECT DISTINCT products.id, products.name, products.description, products.photo, products.price, GROUP_CONCAT(DISTINCT(categories.name)) as categories, COUNT(products.variation) as variation 
            FROM category_products
            INNER JOIN categories ON categories.id = category_products.category_id
            INNER JOIN products ON products.id = category_products.product_id ";
        $queryEnd = " GROUP by products.name  
            ORDER BY `products`.`name` ASC
            LIMIT ? OFFSET ?";

        if($request->id){
            $ids = explode(',',$request->id);
            $in = join(',', array_fill(0, count($ids), '?'));

            $result = DB::select("SELECT COUNT(DISTINCT products.name) as count FROM category_products
            INNER JOIN products ON category_products.product_id = products.id
            WHERE category_id IN ({$in})",$ids);

            $lastPage = ceil($result[0]->count / $perPage);

            array_push($ids,$perPage,$offset);
        
            $products = DB::select($queryBeg."WHERE category_products.category_id IN ({$in})".$queryEnd,$ids); 

        }else{
            $products = DB::select($queryBeg.$queryEnd, [$perPage,$offset]);
        }
        
        $categories = DB::select("SELECT categories.id,categories.name, COUNT(*) as product_count 
        FROM category_products
        INNER JOIN categories ON categories.id = category_products.category_id
        GROUP BY category_id");

        
        return [
            'products'    => $products,
            'current_page' => $currentPage,
            'last_page' => $lastPage,
            'categories' => $categories
        ];
    }
    public function view(Request $request){
        $products = DB::select("SELECT * FROM products WHERE id = ? OR product_id = ?",array($request->id,$request->id));
        $categories = DB::select("SELECT categories.id, categories.name FROM `categories`
        INNER JOIN category_products ON categories.id = category_products.category_id
        WHERE category_products.product_id = ?", array($request->id));

        return [
            'product'=>$products,
            'categories'=>$categories
        ];
    }

    public function search(Request $request){

        $currentPage = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        $perPage = 9;
        $offset = ($currentPage-1)*$perPage;

        $products = DB::select("SELECT DISTINCT products.id, products.name, products.description, products.photo, products.price, GROUP_CONCAT(DISTINCT(categories.name)) as categories, COUNT(products.variation) as variation 
            FROM category_products
            INNER JOIN categories ON categories.id = category_products.category_id
            INNER JOIN products ON products.id = category_products.product_id
            WHERE products.name LIKE '%".$request->id."%'
            GROUP by products.name  
            ORDER BY `products`.`name` ASC");
            
        return $products;
}

    public function addToCart(Request $request)
    {
        $product = DB::select("SELECT * FROM products WHERE id = ?",array($request->id));
        $user = Auth::user();
 
        if(!$product || !$user) {
 
            abort(404);
 
        }

        $user_product = DB::select('SELECT * FROM user_products
        INNER JOIN products ON products.id = user_products.product_id
        WHERE user_products.product_id = ? AND user_products.user_id = ?',array($request->id,$user->id));

        if(!$user_product){
            DB::insert('INSERT into user_products (product_id, user_id, quantity) values (?, ?, ?)', [$request->id,$user->id,1]); // if cart is empty then this the first product

            if(!$product[0]->product_id){
                return [
                    'type' => 'success',
                    'msg' => "Product '".$product[0]->name."' added to cart successfully!"
                ];
            }else{
                return [
                    'type' => 'success',
                    'msg' => "Product '".$product[0]->name." - ".$product[0]->variation."' added to cart successfully!"
                ];
            }

            

        }else{
            DB::update('UPDATE user_products set quantity = quantity + 1 where product_id = ? AND user_id = ?',array($request->id,$user->id));

            return [
                'type' => 'success',
                'msg' => "Product '".$product[0]->name."' added to cart successfully!"
            ];
        }
    }
    public function removeFromCart(Request $request){
        $product = DB::select("SELECT * FROM products WHERE id = ?",array($request->id));
        $user = Auth::user();
 
        if(!$product || !$user) {
 
            abort(404);
 
        }

        $user_product = DB::select('SELECT * FROM user_products
        INNER JOIN products ON products.id = user_products.product_id
        WHERE user_products.product_id = ? AND user_products.user_id = ?',array($request->id,$user->id));
        

        if($user_product[0]) {
            
            if($user_product[0]->quantity < 2){
                DB::delete('DELETE from user_products 
                WHERE user_products.product_id = ? AND user_products.user_id = ?',array($request->id,$user->id)); // IF QUANTITY 1 , then remove
                return "Product removed";
            }
            DB::update('UPDATE user_products 
            SET quantity = quantity - 1 
            WHERE product_id = ? AND user_id = ?',array($request->id,$user->id)); // REMOVE 1 
            return "Product removed";
        }
    }
    public function getCart(){
        $user = Auth::user();

        $products = DB::select('SELECT * FROM user_products
        INNER JOIN products ON products.id = user_products.product_id
        WHERE user_products.user_id = ?',array($user->id));

        return $products;
    }
    public function dumpCart(){
        $user = Auth::user();

        DB::delete('DELETE from user_products 
        WHERE user_products.user_id = ?',array($user->id)); // DELETE ALL PRODUCTS OF USER
        
        return "Products removed";
    }

}