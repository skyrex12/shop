const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');

//Plugins
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const RemovePlugin = require('remove-files-webpack-plugin');

module.exports = merge(common, {
	mode: 'production',
	output:{
		path: path.resolve(__dirname,'./public/build'),
		filename: 'js/app.[hash].js',
	},
	optimization: {
		minimizer: [new OptimizeCSSAssetsPlugin({}),new UglifyJsPlugin()],
	},
	module: {
		rules: [
			{
				test: /\.(js|vue)$/,
				loader: 'eslint-loader',
				enforce: 'pre',
				options: {
					formatter: require('eslint-friendly-formatter'),
					fix: true
				}
			},
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env'],
						plugins: ['@babel/plugin-transform-runtime']
					}
				}
			},
			{ // Fonts
				test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[hash].[ext]',
							outputPath: './fonts/'
						}
					}
				]
			},
			{ // Images
				test: /\.(png|jpe?g|gif)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[hash].[ext]',
							outputPath: './images/'
						},
					},
				]
			},
			{ // CSS
				test: /\.(sa|sc|c)ss$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							publicPath: '../'
						}
					},
					'css-loader',
					'sass-loader',
				],
			},
			{ // Vue
				test: /\.vue$/,
				loader: 'vue-loader'
			}
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: 'css/app.[hash].css',
			chunkFilename: 'css/app.[id].[hash].css',
		}),
		new VueLoaderPlugin({}),
		new UglifyJsPlugin({}),
		new ManifestPlugin({
			publicPath: './build/',
			fileName: '../manifest.json'
		}),
		new RemovePlugin({
			before: {
				include: ['./public/build']
			},
		})
	],
});