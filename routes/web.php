<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MainController@index')->name('main');

Route::group(['middleware'=> 'auth'], function(){
    Route::get('/cart', 'ProductController@getCart');
    Route::get('/cart/flush', 'ProductController@dumpCart');
    Route::get('/products/add-to-cart/{id}','ProductController@addToCart');
    Route::get('/cart/remove-from-cart/{id}','ProductController@removeFromCart');
    Route::get('/getUser','UserController@user');  

    Route::post('/cart/merge', 'SavedProductController@mergeCart');
    Route::post('/cart/replace', 'SavedProductController@replaceCart');
});

Route::get('/getWishlist/{wishList}', 'SavedProductController@displayWishList');

Auth::routes();
