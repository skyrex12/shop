import VueRouter from 'vue-router';

//components
import Product from 'components/Product';
import Products from 'components/Products';
import Cart from 'components/Cart';
import Register from 'components/Register';
import Wishlist from 'components/Wishlist';

let routes = [
	{
		path: '/',
		name: 'products',
		component: Products
	},
	{
		path: '/cart',
		name: 'cart',
		component: Cart,
	},
	{
		path: '/register',
		name: 'register',
		component: Register,
	},
	{
		path: '/product/:id',
		name: 'product',
		component: Product
	},
	{
		path: '/wishlist/:wishlist',
		name: 'displayWishList',
		component: Wishlist
	}
];

export default new VueRouter({
	routes
});