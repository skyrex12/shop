const path = require('path');

module.exports = {
	performance: { hints: false },
	entry:{
		app : [
			'./resources/sass/app.scss',
			'./resources/js/app.js'
		]
	},
	resolve:{
		alias: {
			Fonts: path.resolve('./resources/fonts'),
			Images: path.resolve('./resources/images'),
			'vue$': 'vue/dist/vue.min.js',
			components : path.resolve('./resources/js/components')
		},
		extensions: ['*', '.js', '.vue', '.json']
	},
};