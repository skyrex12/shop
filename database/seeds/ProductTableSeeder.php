<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'Rolex',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/watch1.png',
            'price' => 2698.88
        ]);  
        Product::create([
             'name' => 'Lamp',
             'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
             'photo' => 'img/misc1.png',
             'price' => 20.00
         ]);  
         Product::create([
             'name' => 'Supreme Cap',
             'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
             'photo' => 'img/clothes1.png',
             'price' => 675.00
         ]);
  
         Product::create([
             'name' => 'Louis Vuitton Bag',
             'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
             'photo' => 'img/clothes2.png',
             'price' => 1059.99
         ]);
  
         Product::create([
             'name' => 'Ray-Ban Glasses',
             'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
             'photo' => 'img/clothes3.png',
             'price' => 68.00
         ]);
  
         Product::create([
             'name' => 'Adidas EQT',
             'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
             'photo' => 'img/clothes4.png',
             'price' => 129.99
         ]);

         Product::create([
            'name' => 'Casio watch',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/watch2.png',
            'price' => 49.99
        ]);

        Product::create([
            'name' => 'Computer mouse',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/misc2.jpg',
            'price' => 19.99
        ]);
        Product::create([
            'name' => 'Fairy Multi-Clean',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/misc3.jpg',
            'price' => 2.99
        ]);
        Product::create([
            'name' => 'Bad browser',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/misc4.png',
            'price' => 0.01
        ]);
        Product::create([
            'name' => 'All-purpose tool',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/tools1.jpg',
            'price' => 19.99
        ]);
        Product::create([
            'name' => 'Screwdriver',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/tools2.jpg',
            'price' => 5.00
        ]);
        Product::create([
            'name' => 'Tool storage',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/tools3.jpg',
            'price' => 69.20
        ]);
        Product::create([
            'name' => 'Wrench',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/tools4.jpg',
            'price' => 15.99
        ]);
        Product::create([
            'name' => 'Bulgarke',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/tools5.jpg',
            'price' => 145.99
        ]);
        Product::create([
            'name' => 'Apap',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/supplements1.jpg',
            'price' => 12.99
        ]);
        Product::create([
            'name' => 'Smecta',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/supplements2.jpg',
            'price' => 2.99
        ]);
        Product::create([
            'name' => 'Codeine',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/supplements3.jpg',
            'price' => 12.00
        ]);
        Product::create([
            'name' => 'Protein',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/supplements4.png',
            'price' => 22.99
        ]);
        Product::create([
            'name' => 'Actimel',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/supplements5.jpg',
            'price' => 0.99
        ]);
        Product::create([
            'name' => 'Rolex MS 5245',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/watch3.jpg',
            'price' => 2562.99
        ]);
        Product::create([
            'name' => 'Quartz Rebirth MOCK',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/watch4.jpg',
            'price' => 599.99
        ]);
        Product::create([
            'name' => 'Straton MX-5',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/watch5.jpg',
            'price' => 1049.99
        ]);
        Product::create([
            'name' => 'Adidas Shirt',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/clothes5.jpg',
            'price' => 2.99
        ]);
        Product::create([
            'name' => 'Tattoo',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/misc5.jpg',
            'price' => 12.99
        ]);
        Product::create([
            'name' => 'Rolex',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/watch1.png',
            'variation'=>'white',
            'product_id'=>'1',
            'price' => 2698.88
        ]);
        Product::create([
            'name' => 'Rolex',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/watch1.png',
            'variation'=>'brown',
            'product_id'=>'1',
            'price' => 2698.88
        ]);
        Product::create([
            'name' => 'Louis Vuitton Bag',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/clothes2.png',
            'variation' => 'black',
            'product_id' => '4',
            'price' => 1059.99
        ]);
        Product::create([
            'name' => 'Louis Vuitton Bag',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'photo' => 'img/clothes2.png',
            'variation' => 'white',
            'product_id' => '4',
            'price' => 1059.99
        ]);      
    }
}
