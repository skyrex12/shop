<?php

use App\CategoryProduct;
use Illuminate\Database\Seeder;

class CategoryProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            CategoryProduct::create([
                'product_id' => '1',
                'category_id'=> '1'
            ]);
            CategoryProduct::create([
                'product_id' => '2',
                'category_id'=> '5'
            ]);
            CategoryProduct::create([
                'product_id' => '3',
                'category_id'=> '2'
            ]);
            CategoryProduct::create([
                'product_id' => '4',
                'category_id'=> '2'
            ]);
            CategoryProduct::create([
                'product_id' => '5',
                'category_id'=> '2'
            ]);
            CategoryProduct::create([
                'product_id' => '6',
                'category_id'=> '2'
            ]);
            CategoryProduct::create([
                'product_id' => '7',
                'category_id'=> '1'
            ]);
            CategoryProduct::create([
                'product_id' => '8',
                'category_id'=> '5'
            ]);
            CategoryProduct::create([
                'product_id' => '9',
                'category_id'=> '5'
            ]);
            CategoryProduct::create([
                'product_id' => '10',
                'category_id'=> '5'
            ]);
            CategoryProduct::create([
                'product_id' => '11',
                'category_id'=> '3'
            ]);
            CategoryProduct::create([
                'product_id' => '12',
                'category_id'=> '3'
            ]);
            CategoryProduct::create([
                'product_id' => '13',
                'category_id'=> '3'
            ]);
            CategoryProduct::create([
                'product_id' => '14',
                'category_id'=> '3'
            ]);
            CategoryProduct::create([
                'product_id' => '15',
                'category_id'=> '3'
            ]);
            CategoryProduct::create([
                'product_id' => '16',
                'category_id'=> '4'
            ]);
            CategoryProduct::create([
                'product_id' => '17',
                'category_id'=> '4'
            ]);
            CategoryProduct::create([
                'product_id' => '18',
                'category_id'=> '4'
            ]);
            CategoryProduct::create([
                'product_id' => '19',
                'category_id'=> '4'
            ]);
            CategoryProduct::create([
                'product_id' => '20',
                'category_id'=> '4'
            ]);
            CategoryProduct::create([
                'product_id' => '21',
                'category_id'=> '1'
            ]);
            CategoryProduct::create([
                'product_id' => '22',
                'category_id'=> '1'
            ]);
            CategoryProduct::create([
                'product_id' => '23',
                'category_id'=> '1'
            ]);
            CategoryProduct::create([
                'product_id' => '24',
                'category_id'=> '2'
            ]);
            CategoryProduct::create([
                'product_id' => '25',
                'category_id'=> '5'
            ]);
            CategoryProduct::create([
                'product_id' => '25',
                'category_id'=> '2'
            ]);
            CategoryProduct::create([
                'product_id' => '26',
                'category_id'=> '1'
            ]);
            CategoryProduct::create([
                'product_id' => '27',
                'category_id'=> '1'
            ]);
            CategoryProduct::create([
                'product_id' => '28',
                'category_id'=> '2'
            ]);
            CategoryProduct::create([
                'product_id' => '29',
                'category_id'=> '2'
            ]);
        }
    }
}
