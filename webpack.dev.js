const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');
const ManifestPlugin = require('webpack-manifest-plugin');

//Plugins
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = merge(common, {
	mode: 'development',
	output:{
		path: path.resolve(__dirname,'./public/'),
		filename: './js/app.js',
	},
	module: {
		rules: [
			{
				test: /\.(js|vue)$/,
				loader: 'eslint-loader',
				enforce: 'pre',
				options: {
					formatter: require('eslint-friendly-formatter'),
					fix: true
				}
			},
			{ // Fonts
				test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							outputPath: './fonts/'
						}
					}
				]
			},
			{ // Images
				test: /\.(png|jpe?g|gif)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							outputPath: './images/'
						},
					},
				]
			},
			{ // CSS
				test: /\.(sa|sc|c)ss$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							publicPath: '../'
						}
					},
					'css-loader',
					'sass-loader',
				],
			},
			{ // Vue
				test: /\.vue$/,
				loader: 'vue-loader'
			},
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env'],
						plugins: ['@babel/plugin-transform-runtime']
					}
				}
			},
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: './css/app.css',
			chunkFilename: './css/app.[id].css',
		}),
		new VueLoaderPlugin({}),
		new ManifestPlugin({
			fileName: 'manifest.json'
		})
	],
});